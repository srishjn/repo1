package org.arise.audiolearning.util;

import android.speech.tts.UtteranceProgressListener;
import android.util.Log;

import org.arise.audiolearning.*;
public class UtteranceComplete extends UtteranceProgressListener {
	public boolean firstDone;
	private MainActivity mainAct;
	
	public UtteranceComplete(MainActivity mainAct) {
		super();
		this.firstDone = false;
		this.mainAct = mainAct;
		Log.d("UtteranceComplete", "constructor");
	}

	@Override
	public void onStart(String utteranceId) {
		Log.d("UtteranceComplete", "onStart");
	}

	@Override
	public void onDone(String utteranceId) {
		Log.d("UtteranceComplete", "onDone");
		if(!this.firstDone){
			this.firstDone = true;
			this.mainAct.initialize();
		}
	}

	@Override
	public void onError(String utteranceId) {
		// TODO Auto-generated method stub

	}

}
