package org.arise.audiolearning;

import java.util.HashMap;
import java.util.Locale;

import org.arise.audiolearning.util.SystemUiHider;
import org.arise.audiolearning.util.UtteranceComplete;

import android.annotation.TargetApi;
import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.speech.tts.TextToSpeech.OnUtteranceCompletedListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 * 
 * @see SystemUiHider
 */
public class MainActivity extends Activity implements OnClickListener{
	/**
	 * Whether or not the system UI should be auto-hidden after
	 * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
	 */
	private static final boolean AUTO_HIDE = true;

	/**
	 * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
	 * user interaction before hiding the system UI.
	 */
	private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

	/**
	 * If set, will toggle the system UI visibility upon interaction. Otherwise,
	 * will show the system UI visibility upon interaction.
	 */
	private static final boolean TOGGLE_ON_CLICK = true;

	/**
	 * The flags to pass to {@link SystemUiHider#getInstance}.
	 */
	private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;

	private static final int AUDIO_STOPPED = 0;
	private static final int AUDIO_PLAYING = 1;
	private static final int AUDIO_PAUSED = 2;

	/**
	 * The instance of the {@link SystemUiHider} for this activity.
	 */
	private SystemUiHider mSystemUiHider;
	private Button btn;
	private TextToSpeech tts;
	private String[] lectures;
	private int currentLecture;
	private MediaPlayer player;
	private int audioState;
	private UtteranceComplete utter;
	private HashMap<String, String> map;
	private boolean firstDone;
	private boolean welcomeDone;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		//final View controlsView = findViewById(R.id.fullscreen_content_controls);
		player = MediaPlayer.create(getApplicationContext(), R.raw.lingstart1);
		currentLecture = 0;
		lectures = new String[]{"English 1", "Chemistry 1", "Mathematics 3", "Geography 2"};
		map = new HashMap<String, String>();
		final String instructions = "Welcome! Use the Volume up and Volume Down buttons to scroll through the list of lectures. Tap on the screen to play or pause the lecture. Now Tap on the screen once to begin.";
		//final String instructions = "Welcome!";
		//utter = new UtteranceComplete(this);
		firstDone = false;
		welcomeDone = false;
		tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
			
			@Override
			public void onInit(int status) {
				Log.d("MainActivity", "tts init");
				tts.setLanguage(Locale.UK);
				tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
					
					@Override
					public void onStart(String utteranceId) {
						Log.d("UtteranceComplete", "onStart");
					}

					@Override
					public void onDone(String utteranceId) {
						Log.d("UtteranceComplete", "onDone");
						if(!firstDone){
							firstDone = true;
							runOnUiThread(new Runnable() {
								
								@Override
								public void run() {
									initialize();
								}
							});
						}
					}
					
					@Override
					public void onError(String utteranceId) {
						// TODO Auto-generated method stub
						
					}
				});
				map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "welcome");
				tts.speak(instructions, TextToSpeech.QUEUE_FLUSH, map);		
				//tts.speak(lectures[currentLecture], TextToSpeech.QUEUE_FLUSH, null);		
			}
		});
		Log.d("MainActivity", "tts vars set");
		btn = (Button)findViewById(R.id.button1);
		btn.setText("Welcome!!");
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		// Trigger the initial hide() shortly after the activity has been
		// created, to briefly hint to the user that UI controls
		// are available.
		//delayedHide(100);
	}
	
	public void initialize(){
		btn.setOnClickListener(this);
		currentLecture = 0;
		//speakLectureName();
	}

	@Override
	public void onClick(View v) {
		if(!welcomeDone){
			welcomeDone = true;
			speakLectureName();
			return;
		}
		if(player.isPlaying()){
			player.pause();
		}
		else{
			player.start();
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event){
		boolean speak = false;
		if(firstDone){
			if(keyCode == KeyEvent.KEYCODE_VOLUME_UP){
				currentLecture = (currentLecture + 1) % lectures.length;
				speak = true;
			}
			else if(keyCode == KeyEvent.KEYCODE_VOLUME_DOWN){
				currentLecture = (currentLecture - 1);
				if(currentLecture < 0)
					currentLecture = lectures.length - 1;
				speak = true;
			}
			if(speak){
				speakLectureName();
			}
		}
		return true;
	}
	
	public void speakLectureName(){
		btn.setText(lectures[currentLecture]);
		tts.speak(lectures[currentLecture], TextToSpeech.QUEUE_FLUSH, null);
		if(player.isPlaying())
			player.stop();
		player.reset();
		player = MediaPlayer.create(getApplicationContext(), R.raw.lingstart1);
	}
}
